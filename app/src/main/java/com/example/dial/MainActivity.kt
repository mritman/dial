package com.example.dial

import android.Manifest.permission.CALL_PHONE
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.PermissionChecker
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.core.net.toUri

class MainActivity : AppCompatActivity() {

    private val requestPermission = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val phoneNumberText: EditText = findViewById(R.id.phoneNumberText)

        val button: Button = findViewById(R.id.callButton)

        button.setOnClickListener {
            val phoneNumber = phoneNumberText.text.toString()
            call(phoneNumber)
        }
    }

    private fun call(phoneNumber: String) {
        if (callPermissionGranted()) {
            val uri = toUri(phoneNumber)
            startActivity(Intent(Intent.ACTION_CALL, uri))
        } else {
            requestPermissions(arrayOf(CALL_PHONE), requestPermission)
        }
    }

    private fun toUri(phoneNumber: String) = "tel:${phoneNumber}".toUri()

    private fun callPermissionGranted() =
        PermissionChecker.checkSelfPermission(this, CALL_PHONE) == PERMISSION_GRANTED

}