package com.example.dial

import android.telecom.Call
import android.telecom.VideoProfile

object OngoingCall {

    private val stateListeners: MutableSet<(Int) -> Unit> = mutableSetOf()

    private val callback = object : Call.Callback() {

        override fun onStateChanged(call: Call?, state: Int) {
            super.onStateChanged(call, state)
            stateListeners.toSet().forEach { it.invoke(state) }
        }

    }

    private var call: Call? = null

    fun callState() : Int? = call?.state

    fun displayName() : String? = call?.details?.handle.toString()

    fun addStateListener(listener: (Int) -> Unit) {
        stateListeners.add(listener)
    }

    fun start(newCall: Call) {
        call = newCall
        call?.registerCallback(callback)
    }

    fun end() {
        call?.unregisterCallback(callback)
        call = null
    }

    fun answer() {
        call?.answer(VideoProfile.STATE_AUDIO_ONLY)
    }

    fun hangup() {
        call?.disconnect()
    }

}
