package com.example.dial

import android.content.Intent
import android.telecom.Call
import android.telecom.InCallService


class DialCallService : InCallService() {

    override fun onCallAdded(call: Call) {
        OngoingCall.start(call)

        // TODO only do this when App is already in the foreground, show notification otherwise
        val dialogIntent = Intent(this, CallActivity::class.java)
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(dialogIntent)
    }

    override fun onCallRemoved(call: Call) {
        OngoingCall.end()
    }

}