package com.example.dial

import android.telecom.Call.*

private val STATE_MAP = mapOf(
    STATE_NEW to CallStatus.DIALING,
    STATE_DIALING to CallStatus.DIALING,
    STATE_CONNECTING to CallStatus.DIALING,
    STATE_RINGING to CallStatus.RINGING,
    STATE_ACTIVE to CallStatus.CONNECTED,
    STATE_DISCONNECTING to CallStatus.DISCONNECTING,
    STATE_DISCONNECTED to CallStatus.DISCONNECTED)

fun toCallStatus(state: Int) : CallStatus {
    return STATE_MAP[state] ?: CallStatus.UNKNOWN
}