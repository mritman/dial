package com.example.dial

import android.os.Bundle
import android.telecom.Call
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class CallActivity : AppCompatActivity() {

    private lateinit var answerButton: Button
    private lateinit var hangUpButton: Button
    private lateinit var callStatus: TextView
    private lateinit var callDetails: TextView
    private lateinit var additionalCallDetails: TextView

    private lateinit var detailLookupService:  NumberDetailsLookupService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call)

        answerButton = findViewById(R.id.answerButton)
        hangUpButton = findViewById(R.id.hangUpButton)
        callStatus = findViewById(R.id.callStatus)
        callDetails = findViewById(R.id.callDetails)
        additionalCallDetails = findViewById(R.id.additionalCallDetails)

        answerButton.setOnClickListener {
            OngoingCall.answer()
        }

        hangUpButton.setOnClickListener {
            OngoingCall.hangup()
        }

        OngoingCall.addStateListener {
            val newStatus = toCallStatus(it)
            updateView(newStatus)

            if (it == Call.STATE_DISCONNECTED) {
                finish()
            }
        }

        detailLookupService = NumberDetailsLookupService(this)
    }

    override fun onStart() {
        super.onStart()

        val callState = OngoingCall.callState()
        if (callState != null) {
            val newStatus = toCallStatus(callState)
            updateView(newStatus)
            updateAdditionalCallDetails()
        }
    }

    private fun updateAdditionalCallDetails() {
        detailLookupService.lookup { additionalCallDetails.text = it }
    }

    override fun onStop() {
        detailLookupService.stop()
        super.onStop()
    }

    private fun updateView(newStatus: CallStatus) {
        updateStatusView(newStatus)
        updateDetailsView()
        updateButtons(newStatus)
    }

    private fun updateStatusView(newStatus: CallStatus) {
        callStatus.text = newStatus.asString()
    }

    private fun updateDetailsView() {
        callDetails.text = OngoingCall.displayName() ?: "Details"
    }

    private fun updateButtons(newStatus: CallStatus) {
        answerButton.isEnabled = newStatus == CallStatus.RINGING
        hangUpButton.isEnabled = setOf(CallStatus.RINGING, CallStatus.CONNECTED).contains(newStatus)
    }

}