package com.example.dial

import android.content.Context
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson

private const val API_URL = "https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=1"

private const val REQUEST_TAG = "numberLookup"

class NumberDetailsLookupService(context: Context) {

    private val queue = Volley.newRequestQueue(context)
    private val gson = Gson()

    fun lookup(callback: (String) -> Unit) {
        // TODO lookup actual additional details using a phone number instead of a random fact.
        val stringRequest = StringRequest(
            Request.Method.GET,
            API_URL,
            Response.Listener<String> { response ->
                val fact = gson.fromJson(response, Fact::class.java)
                callback.invoke(fact?.text ?: "No facts found")
            },
            Response.ErrorListener { callback.invoke("That didn't work!") })

        stringRequest.tag = REQUEST_TAG

        queue.add(stringRequest)
    }

    fun stop() {
        queue.cancelAll(REQUEST_TAG)
    }

}

data class Fact(val _id: String, val text: String)