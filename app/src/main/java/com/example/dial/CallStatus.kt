package com.example.dial

enum class CallStatus(private val stringValue: String) {
    DIALING("Dialing"),
    RINGING("Incoming call"),
    CONNECTED("Connected"),
    DISCONNECTING("Disconnecting"),
    DISCONNECTED("Disconnected"),
    UNKNOWN("Unknown");

    fun asString() = stringValue

}